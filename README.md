# game_2048

This repository contains the __game_2048__ rush of 42

Usage:  
	
	make
	./game_2048  

Enjoy it!  

![2048.png](https://raw.githubusercontent.com/dvolberg/2048/master/2048.png)
![game.png](https://raw.githubusercontent.com/dvolberg/2048/master/game.png)
